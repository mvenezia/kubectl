FROM alpine
ENV K8S_RELEASE=v1.11.0
ENV K8S_RELEASE_URL=http://storage.googleapis.com/kubernetes-release/release/${K8S_RELEASE}/bin/linux/amd64/kubectl

MAINTAINER Michael Venezia <mvenezia@gmail.com>

RUN wget ${K8S_RELEASE_URL} && \
    chmod a+x /kubectl

ENTRYPOINT ["/kubectl"]